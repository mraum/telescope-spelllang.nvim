local has_telescope, telescope = pcall(require, "telescope")
if not has_telescope then
  error(
    "This plugin requires telescope.nvim (https://github.com/nvim-telescope/telescope.nvim)"
  )
end

local spelllang_picker = require("telescope-spelllang").picker

return telescope.register_extension({
  exports = {
    spelllang = spelllang_picker,
  },
})
