local vim = vim

local finders = require("telescope.finders")
local pickers = require("telescope.pickers")
local actions = require("telescope.actions")
local actions_state = require("telescope.actions.state")
local telescope_config = require("telescope.config").values


local M = {}

local defaults = {
  languages = {
    {label = "English", dict = "en_us"},
  },
}

M.options = {}

M.setup = function (opts)
  opts = opts or {}
  M.options = vim.tbl_deep_extend("force", defaults, opts)
end

local set_spelllang = function (prompt_bufnr)
  local dict = actions_state.get_selected_entry().value.dict
  actions.close(prompt_bufnr)
  vim.wo.spell = true
  vim.bo.spelllang = dict
end

M.picker = function (opts)
  pickers
    .new(opts, {
      prompt_title = "Spell Languages",
      finder = finders.new_table({
        results = M.options.languages,
        entry_maker = function (langspec)
          return {
            value   = langspec,
            ordinal = langspec.label,
            display = langspec.label,
          }
        end,
      }),
      sorter = telescope_config.generic_sorter(opts),
      previewer = false,
      attach_mappings = function ()
        actions.select_default:replace(set_spelllang)
        return true
      end,
    })
    :find()
end

return M
